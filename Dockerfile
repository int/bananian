FROM debian:bookworm
COPY ./bananian-archive.gpg /etc/apt/trusted.gpg.d/
COPY ./bananian-archive.list /etc/apt/sources.list.d/
RUN dpkg --add-architecture armhf
